FROM debian:bullseye-slim
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get  update && \
  apt-get install -y \
    python3-prometheus-client \
    python3-requests \
    python3-pydantic

RUN mkdir /usr/local/gralava
COPY config.py gralava.py /usr/local/gralava/

ENTRYPOINT ["python3", "/usr/local/gralava/gralava.py"]
