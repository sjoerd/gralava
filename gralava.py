# Copyright © 2020 Collabora, Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Author: Daniel Stone <daniels@collabora.com>

import json
import os
import sys
import time
import types
import urllib.parse

import requests
import prometheus_client
from prometheus_client.core import GaugeMetricFamily, REGISTRY

from config import settings

class LAVADispatcher(types.SimpleNamespace):
        def __init__(self, server, d):
                super().__init__(**d)
                self.devices = []

class LAVADeviceType(types.SimpleNamespace):
        def __init__(self, server, d):
                super().__init__(**d)
                self.devices = []

class LAVADevice(types.SimpleNamespace):
        def __init__(self, server, d):
                super().__init__(**d)
                self.is_production = server.is_production_host(self.worker_host)
                self.tags = [server.tags[tag].name for tag in self.tags]

class LAVATag(types.SimpleNamespace):
        def __init__(self, server, d):
                super().__init__(**d)

class LAVAServer():
        def __init__(self, settings):
                self.settings = settings

        def is_production_host(self, host):
                host = host.replace(".cbg.collabora.co.uk", "")
                host = host.replace(".collabora.co.uk", "")
                if host == "baobab":
                        return True
                if host.startswith("lava-rack"):
                        return True
                return False

        def request_one(self, endpoint, params):
                uri = "{}/api/v0.2/{}".format(self.settings.lava_uri, endpoint)
                headers = {
                        "Authorization": "Token {}".format(self.settings.lava_token.get_secret_value()),
                        "User-Agent": "gralava",
                        "Content-Type": "application/json",
                }
                r = requests.get(uri, headers=headers, params=params)
                r.raise_for_status()
                return r.json()

        def request_full_list(self, endpoint, key, decodeclass, params={}):
                # This method was written to support pagination; unfortunately LAVA's
                # REST pagination is thoroughly broken
                params["limit"] = 5000
                ret = {}
                while True:
                        r = self.request_one(endpoint, params)
                        ret.update({v[key]: decodeclass(self, v) for v in r['results']})
                        if not r['next']:
                                return ret
                        params = urllib.parse.parse_qs(urllib.parse.urlparse(r['next']).query)

        def refresh(self):
                self.tags = self.request_full_list("tags", "id", LAVATag)
                self.dispatchers = self.request_full_list("workers", "hostname", LAVADispatcher)
                self.device_types = self.request_full_list("devicetypes", "name", LAVADeviceType)
                self.devices = self.request_full_list("devices", "hostname", LAVADevice)
                for (name, t) in self.device_types.items():
                        t.devices = { n: d for (n,d) in self.devices.items() if d.device_type == name and d.is_production }
                        t.num_devices = sum(1 for d in t.devices.values())
                        t.num_healthy = sum(1 for d in t.devices.values() if d.health == 'Good')
                        if t.num_devices == 0:
                                continue
                        print("{}: {} / {} online".format(name, t.num_healthy, t.num_devices))
                        for d in t.devices.values():
                                print("    {}: {}".format(d.hostname, d.health))


class LAVACollector(object):
        def __init__(self, settings):
                self.settings = settings
                self.server = LAVAServer(self.settings)

        def collect(self):
                self.server.refresh()
                type_number = GaugeMetricFamily("device_type_number_online",
                                                "Number of device type online",
                                                labels=["device_type"]) 
                type_percent = GaugeMetricFamily("device_type_percent_online",
                                                 "Percentage of device type online",
                                                 labels=["device_type"]) 
                for (name, t) in self.server.device_types.items():
                        if t.num_devices == 0:
                                continue
                        type_number.add_metric([name], t.num_healthy)
                        type_percent.add_metric([name], t.num_healthy / t.num_devices)
                yield type_number
                yield type_percent

if __name__ == '__main__':
        REGISTRY.register(LAVACollector(settings))
        prometheus_client.start_http_server(settings.gralava_port)
        while True:
                time.sleep(60)
